import 'package:roazhon_star/screens/home/home_screen.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static const String homeRoute = '/';
  static const String seriesRoute = '/series';
  static const String settingsRoute = '/settings';
  static const String aboutRoute = '/about';
  static const String bookRoute = '/book';

  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeRoute:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => HomeScreen(),
        );
      // case bookRoute:
      //   return MaterialPageRoute(
      //     settings: settings,
      //     builder: (_) => BooksDetailsScreen(),
      //   );
      default:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => HomeScreen(),
        );
    }
  }
}
