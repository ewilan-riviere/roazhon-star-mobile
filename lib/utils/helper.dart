import 'dart:convert';

import 'package:flutter/material.dart';

class Helper {
  static Color hexaToColor(String hexa) {
    var originalColor = '0xff' + hexa.replaceAll('#', '');

    return Color(int.parse(originalColor));
  }

  /// Print pretty JSON from [String]
  static void prettyPrintJson(String input) {
    const decoder = JsonDecoder();
    var encoder = JsonEncoder.withIndent('  ');
    final dynamic object = decoder.convert(input);
    final dynamic prettyString = encoder.convert(object);
    prettyString.split('\n').forEach((dynamic element) => print(element));
  }

  static void getMessage({
    required BuildContext context,
    required String text,
    MaterialColor? color,
    Duration duration = const Duration(seconds: 4),
    bool noLimit = false,
    SnackBarAction? snackBarAction,
  }) {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        backgroundColor: color,
        margin: const EdgeInsets.all(10.0),
        elevation: 2.0,
        duration: noLimit ? Duration(days: 1) : duration,
        behavior: SnackBarBehavior.floating,
        action: snackBarAction,
      ),
    );
  }

  static void clearMessage({required BuildContext context}) {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
  }
}
