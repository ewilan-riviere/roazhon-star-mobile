// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DatasetFields.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DatasetFields _$DatasetFieldsFromJson(Map<String, dynamic> json) {
  return DatasetFields(
    idbillettique: json['idbillettique'] as int?,
    idparcoursprincipalaller: json['idparcoursprincipalaller'] as String?,
    couleurligne: json['couleurligne'] as String?,
    couleurtexteligne: json['couleurtexteligne'] as String?,
    idparcoursprincipalretour: json['idparcoursprincipalretour'] as String?,
    nomfamillecommerciale: json['nomfamillecommerciale'] as String?,
    nomlong: json['nomlong'] as String?,
    nomcourt: json['nomcourt'] as String?,
    id: json['id'] as String?,
  );
}

Map<String, dynamic> _$DatasetFieldsToJson(DatasetFields instance) =>
    <String, dynamic>{
      'idbillettique': instance.idbillettique,
      'idparcoursprincipalaller': instance.idparcoursprincipalaller,
      'couleurligne': instance.couleurligne,
      'couleurtexteligne': instance.couleurtexteligne,
      'idparcoursprincipalretour': instance.idparcoursprincipalretour,
      'nomfamillecommerciale': instance.nomfamillecommerciale,
      'nomlong': instance.nomlong,
      'nomcourt': instance.nomcourt,
      'id': instance.id,
    };
