import 'package:json_annotation/json_annotation.dart';

part 'DatasetFields.g.dart';

@JsonSerializable()
class DatasetFields {
  DatasetFields({
    this.idbillettique,
    this.idparcoursprincipalaller,
    this.couleurligne,
    this.couleurtexteligne,
    this.idparcoursprincipalretour,
    this.nomfamillecommerciale,
    this.nomlong,
    this.nomcourt,
    this.id,
  });

  int? idbillettique;
  String? idparcoursprincipalaller;
  String? couleurligne;
  String? couleurtexteligne;
  String? idparcoursprincipalretour;
  String? nomfamillecommerciale;
  String? nomlong;
  String? nomcourt;
  String? id;

  factory DatasetFields.fromJson(Map<String, dynamic> json) =>
      _$DatasetFieldsFromJson(json);
  Map<String, dynamic> toJson() => _$DatasetFieldsToJson(this);
}
