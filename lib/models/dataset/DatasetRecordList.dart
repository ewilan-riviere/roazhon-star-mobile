import 'package:json_annotation/json_annotation.dart';
import 'package:roazhon_star/models/dataset/DatasetLink.dart';
import 'package:roazhon_star/models/dataset/DatasetRecord.dart';

part 'DatasetRecordList.g.dart';

@JsonSerializable()
class DatasetRecordList {
  DatasetRecordList({
    required this.links,
    required this.record,
  });

  List<DatasetLink> links;
  DatasetRecord record;

  factory DatasetRecordList.fromJson(Map<String, dynamic> json) =>
      _$DatasetRecordListFromJson(json);
  Map<String, dynamic> toJson() => _$DatasetRecordListToJson(this);
}
