// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Dataset.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Dataset _$DatasetFromJson(Map<String, dynamic> json) {
  return Dataset(
    totalCount: json['total_count'] as int,
    links: (json['links'] as List<dynamic>?)
        ?.map((e) => DatasetLink.fromJson(e as Map<String, dynamic>))
        .toList(),
    records: (json['records'] as List<dynamic>?)
        ?.map((e) => DatasetRecordList.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$DatasetToJson(Dataset instance) => <String, dynamic>{
      'total_count': instance.totalCount,
      'links': instance.links,
      'records': instance.records,
    };
