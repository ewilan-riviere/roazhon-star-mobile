// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DatasetRecordList.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DatasetRecordList _$DatasetRecordListFromJson(Map<String, dynamic> json) {
  return DatasetRecordList(
    links: (json['links'] as List<dynamic>)
        .map((e) => DatasetLink.fromJson(e as Map<String, dynamic>))
        .toList(),
    record: DatasetRecord.fromJson(json['record'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DatasetRecordListToJson(DatasetRecordList instance) =>
    <String, dynamic>{
      'links': instance.links,
      'record': instance.record,
    };
