import 'package:json_annotation/json_annotation.dart';
import 'package:roazhon_star/models/dataset/DatasetFields.dart';

part 'DatasetRecord.g.dart';

@JsonSerializable()
class DatasetRecord {
  DatasetRecord({
    this.id,
    this.timestamp,
    this.size,
    this.fields,
  });

  String? id;
  DateTime? timestamp;
  int? size;
  DatasetFields? fields;

  factory DatasetRecord.fromJson(Map<String, dynamic> json) =>
      _$DatasetRecordFromJson(json);
  Map<String, dynamic> toJson() => _$DatasetRecordToJson(this);
}
