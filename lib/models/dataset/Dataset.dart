import 'package:json_annotation/json_annotation.dart';
import 'package:roazhon_star/models/dataset/DatasetLink.dart';
import 'package:roazhon_star/models/dataset/DatasetRecordList.dart';

part 'Dataset.g.dart';

@JsonSerializable()
class Dataset {
  Dataset({
    this.totalCount = 0,
    this.links,
    this.records,
  });

  @JsonKey(name: 'total_count')
  int totalCount;
  List<DatasetLink>? links;
  List<DatasetRecordList>? records;

  factory Dataset.fromJson(Map<String, dynamic> json) =>
      _$DatasetFromJson(json);
  Map<String, dynamic> toJson() => _$DatasetToJson(this);
}
