import 'package:json_annotation/json_annotation.dart';

part 'DatasetLink.g.dart';

@JsonSerializable()
class DatasetLink {
  DatasetLink({
    required this.href,
    required this.rel,
  });

  String href;
  String rel;

  factory DatasetLink.fromJson(Map<String, dynamic> json) =>
      _$DatasetLinkFromJson(json);
  Map<String, dynamic> toJson() => _$DatasetLinkToJson(this);
}
