// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DatasetLink.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DatasetLink _$DatasetLinkFromJson(Map<String, dynamic> json) {
  return DatasetLink(
    href: json['href'] as String,
    rel: json['rel'] as String,
  );
}

Map<String, dynamic> _$DatasetLinkToJson(DatasetLink instance) =>
    <String, dynamic>{
      'href': instance.href,
      'rel': instance.rel,
    };
