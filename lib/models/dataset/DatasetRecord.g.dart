// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DatasetRecord.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DatasetRecord _$DatasetRecordFromJson(Map<String, dynamic> json) {
  return DatasetRecord(
    id: json['id'] as String?,
    timestamp: json['timestamp'] == null
        ? null
        : DateTime.parse(json['timestamp'] as String),
    size: json['size'] as int?,
    fields: json['fields'] == null
        ? null
        : DatasetFields.fromJson(json['fields'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DatasetRecordToJson(DatasetRecord instance) =>
    <String, dynamic>{
      'id': instance.id,
      'timestamp': instance.timestamp?.toIso8601String(),
      'size': instance.size,
      'fields': instance.fields,
    };
