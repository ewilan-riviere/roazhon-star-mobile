import 'package:roazhon_star/app.dart';
import 'package:roazhon_star/providers/preferences_provider.dart';
import 'package:roazhon_star/routes/app_router.dart';
import 'package:roazhon_star/theme/theme_manager.dart';
import 'package:roazhon_star/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  var _theme;
  Future<String> _settings() async {
    var theme = ThemeNotifier();

    // Get theme of device
    var brightness = SchedulerBinding.instance!.window.platformBrightness;
    var darkModeOn = brightness == Brightness.dark;

    var themeMode = await PreferencesProvider.readData('themeMode');

    // Manage for first init
    if (themeMode == null) {
      if (darkModeOn) {
        theme.setDarkMode();
      } else {
        theme.setLightMode();
      }
    } else {
      if (themeMode == 'light') {
        theme.setLightMode();
      } else {
        theme.setDarkMode();
      }
    }

    _theme = theme.getTheme();

    return '';
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeNotifier>(
      builder: (context, theme, _) => FutureBuilder(
        future: _settings(),
        builder: (context, snapshot) {
          return MaterialApp(
            theme: _theme,
            color: Color(primaryHexa),
            home: _home(snapshot),
            debugShowCheckedModeBanner: false,
            onGenerateRoute: AppRouter.generateRoute,
            initialRoute: '/',
          );
        },
      ),
    );
  }

  Widget _splashScreen() {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image(
              image: AssetImage('assets/images/icon.png'),
              // width: 300,
              height: 150,
              fit: BoxFit.fill,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Text(
                Constants.title,
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }

  _home(AsyncSnapshot snapshot) {
    if (snapshot.hasData) {
      return App();
    } else {
      return _splashScreen();
    }
  }
}
