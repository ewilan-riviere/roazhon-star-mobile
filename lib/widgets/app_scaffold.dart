import 'package:roazhon_star/routes/app_router.dart';
import 'package:roazhon_star/utils/constants.dart';
import 'package:roazhon_star/widgets/app_bottom_nav.dart';
import 'package:flutter/material.dart';

import 'app_drawer.dart';

String title = Constants.title;

class AppScaffold extends StatefulWidget {
  final String? title;
  final bool withDrawer;
  final Widget widget;

  const AppScaffold(
      {Key? key, this.title, required this.widget, this.withDrawer = false})
      : super(key: key);
  @override
  _AppScaffoldState createState() => _AppScaffoldState();
}

class _AppScaffoldState extends State<AppScaffold> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      // drawer: widget.withDrawer ? AppDrawer() : null,
      bottomNavigationBar: AppBottomNav(),
      appBar: AppBar(
        title: Text(
          widget.title ?? title,
          // style: GoogleFonts.handlee(fontSize: 20),
        ),
        backgroundColor: Colors.black,
        elevation: 0,
        actions: <Widget>[
          // IconButton(
          //   icon: Icon(choices[0].icon),
          //   onPressed: () {
          //     _select(choices[0]);
          //   },
          // ),
        ],
      ),
      body: widget.widget,
    );
  }
}
