import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:roazhon_star/models/dataset/Dataset.dart';
import 'package:roazhon_star/repositories/DatasetRepository.dart';
import 'package:roazhon_star/utils/helper.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Future<Dataset>? _futureDataset;
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _futureDataset = DatasetRepository.get();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<Dataset>(
        future: _futureDataset,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var dataset = snapshot.data;
            return Scrollbar(
              isAlwaysShown: true,
              controller: _scrollController,
              // child: ListView.builder(
              // controller: _scrollController,
              // itemCount: dataset!.records!.length,
              // itemBuilder: (context, index) {
              //     var record = dataset.records![index].record;
              //     return ListTile(
              //       title: Text(
              //         record.fields!.nomlong!,
              //       ),
              //       leading: Container(
              //         width: 40,
              //         height: 40,
              // decoration: BoxDecoration(
              //   color: Helper.hexaToColor(record.fields!.couleurligne!),
              //   border: Border.all(
              //     color: Colors.black,
              //     width: 0.8,
              //   ),
              //   borderRadius: BorderRadius.all(
              //     Radius.circular(20),
              //   ),
              // ),
              // child: Align(
              //   alignment: Alignment.center,
              //   child: Text(
              //     record.fields!.nomcourt!,
              //     style: TextStyle(
              //       fontSize: 14.0,
              //       color: Helper.hexaToColor(
              //         record.fields!.couleurtexteligne!,
              //       ),
              //     ),
              //   ),
              // ),
              //       ),
              //     );
              //   },
              // ),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: GridView.builder(
                    // shrinkWrap: true,
                    // physics: NeverScrollableScrollPhysics(),
                    controller: _scrollController,
                    itemCount: dataset!.records!.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 5,
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 10.0,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      var record = dataset.records![index].record;
                      return Container(
                        decoration: BoxDecoration(
                          color: Helper.hexaToColor(
                            record.fields!.couleurligne!,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(999),
                          ),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            record.fields!.nomcourt!,
                            style: TextStyle(
                              fontSize: 26.0,
                              fontWeight: FontWeight.bold,
                              color: Helper.hexaToColor(
                                record.fields!.couleurtexteligne!,
                              ),
                            ),
                          ),
                        ),
                      );
                    }),
              ),
            );
          } else if (snapshot.hasError) {
            return Container(
              child: Text('Data error'),
            );
          } else {
            return Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ),
            );
          }
        },
      ),
    );
  }
}
