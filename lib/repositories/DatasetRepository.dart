import 'package:roazhon_star/models/dataset/Dataset.dart';
import 'package:roazhon_star/providers/api_provider.dart';

class DatasetRepository {
  static Future<Dataset>? get() async {
    var entity = Dataset();

    var response = await ApiProvider().getHTTP(
        '/catalog/datasets/tco-bus-topologie-lignes-td/records?limit=100&pretty=false&timezone=UTC&sort=id');
    var data = response!.data;

    try {
      entity = Dataset.fromJson(data);
    } catch (e) {
      print(e.toString());
      print('ERROR');
      throw ('Error on JSON serializable');
    }

    return entity;
  }

  // Future<List<Dataset>> getSelection() async {
  //   var list = <Book>[];

  //   var response = await ApiProvider().getHTTP(ApiRoutes.booksSelectionRoute);
  //   var data = response!.data['data'];

  //   try {
  //     for (var item in data) {
  //       var book = Book.fromJson(item);
  //       list.add(book);
  //     }
  //   } catch (e) {
  //     print(e.toString());
  //   }

  //   return list;
  // }
}
