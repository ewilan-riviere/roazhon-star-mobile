import 'package:roazhon_star/routes/app_router.dart';
import 'package:roazhon_star/screens/home/home_screen.dart';
import 'package:roazhon_star/utils/constants.dart';
import 'package:roazhon_star/utils/helper.dart';
import 'package:roazhon_star/widgets/app_bottom_nav.dart';
import 'package:flutter/material.dart';

class App extends StatefulWidget {
  App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  late PageController _pageController;
  var _selectedPage = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _selectedPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text(Constants.title),
        backgroundColor: Helper.hexaToColor('#004c9a'),
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        children: <Widget>[
          HomeScreen(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedPage,
        onTap: (value) {
          print(value);
          _pageController.jumpToPage(value);
          setState(() {
            _selectedPage = value;
          });
        },
        items: _list(),
      ),
    );
  }

  List<NavigationBarItem> _list() {
    return [
      NavigationBarItem(
        label: 'Home',
        icon: Icon(
          Icons.home,
        ),
        route: AppRouter.homeRoute,
      ),
      NavigationBarItem(
        label: 'Series',
        icon: Icon(
          Icons.collections,
        ),
        route: AppRouter.seriesRoute,
      ),
    ];
  }
}
