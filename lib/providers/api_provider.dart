import 'package:roazhon_star/utils/constants.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiProvider {
  static Future<String> getApi() async {
    var api = Constants.apiUrl;

    return api;
  }

  static Future<Dio> getDio() async {
    var api = await ApiProvider.getApi();
    var opts = BaseOptions(
      baseUrl: api,
      responseType: ResponseType.json,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      headers: {Headers.acceptHeader: 'application/json'},
    );
    var dio = Dio(opts);

    // dio
    //   ..interceptors.add(
    //     InterceptorsWrapper(onRequest: (options, handler) {
    //       // Do something before request is sent
    //       return handler.next(options); //continue
    //       // If you want to resolve the request with some custom data，
    //       // you can resolve a `Response` object eg: return `dio.resolve(response)`.
    //       // If you want to reject the request with a error message,
    //       // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    //     }, onResponse: (response, handler) {
    //       // Do something with response data
    //       return handler.next(response); // continue
    //       // If you want to reject the request with a error message,
    //       // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    //     }, onError: (DioError e, handler) {
    //       // Do something with response error
    //       return handler.next(e); //continue
    //       // If you want to resolve the request with some custom data，
    //       // you can resolve a `Response` object eg: return `dio.resolve(response)`.
    //     }),
    //   );

    return dio;
  }

  Future<Response?> getHTTP(String url) async {
    try {
      var dio = await getDio();
      var response = await dio.get(url);

      return response;
    } on DioError catch (e) {
      // Log.warn((e.response?.statusCode).toString());
      return e.response;
    }
  }
}
