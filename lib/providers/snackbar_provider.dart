import 'package:roazhon_star/utils/constants.dart';
import 'package:flutter/material.dart';

class SnackbarProvider {
  static void clear(BuildContext context) {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
  }

  /// Show `SnackBar`
  static void show({
    required BuildContext context,
    String? text,
    IconData? icon,
    Color? color,
    Color? bgColor,
    double? elevation,
    Duration? duration,
    bool? noLimit,
    SnackBarAction? action,
    SnackbarType? type,
  }) {
    SnackbarProvider.clear(context);
    if (type != null) {
      var snackbarModel = SnackbarTypeExtension.style(type);
      text = text ?? snackbarModel.text;
      icon = icon ?? snackbarModel.icon;
      color = color ?? snackbarModel.color;
      bgColor = bgColor ?? snackbarModel.bgColor;
      duration = duration ?? snackbarModel.duration;
      noLimit = noLimit ?? snackbarModel.noLimit;
    }

    color = color ?? Colors.white;
    bgColor = bgColor ?? const Color(primaryHexa);
    elevation = elevation ?? 1.0;
    duration = duration ?? const Duration(seconds: 4);
    noLimit = noLimit ?? false;

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Row(
          children: [
            if (icon != null)
              Icon(
                icon,
                color: color,
              ),
            if (icon != null)
              SizedBox(
                width: 20,
              ),
            Flexible(
              child: Text(
                text ?? 'Snackbar',
                maxLines: 2,
                softWrap: false,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  color: color,
                ),
              ),
            ),
          ],
        ),
        backgroundColor: bgColor,
        margin: const EdgeInsets.all(10.0),
        elevation: elevation,
        duration: noLimit ? Duration(days: 1) : duration,
        behavior: SnackBarBehavior.floating,
        action: action,
      ),
    );
  }
}

class SnackbarModel {
  SnackbarModel({
    required this.text,
    this.icon,
    this.color = Colors.white,
    this.bgColor = Colors.grey,
    this.duration = const Duration(seconds: 4),
    this.noLimit = false,
  });

  String text;
  IconData? icon;
  Color? color;
  Color? bgColor;
  Duration? duration;
  bool noLimit;

  static SnackbarModel success({String? text}) {
    return SnackbarModel(
      text: text ?? 'Succès !',
      bgColor: Colors.green,
      icon: Icons.check,
    );
  }

  static SnackbarModel failed({String? text}) {
    return SnackbarModel(
      text: text ?? 'Erreur',
      bgColor: Colors.red,
      icon: Icons.warning,
    );
  }

  static SnackbarModel info({String? text}) {
    return SnackbarModel(
      text: text ?? 'Information',
      bgColor: Colors.blue,
      icon: Icons.info,
    );
  }

  static SnackbarModel wait({String? text}) {
    return SnackbarModel(
      text: text ?? 'Traitement en cours...',
      bgColor: Colors.grey.shade700,
      icon: Icons.hourglass_bottom_rounded,
      noLimit: true,
    );
  }
}

enum SnackbarType {
  success,
  failed,
  info,
  wait,
}

extension SnackbarTypeExtension on SnackbarType {
  static const Map<SnackbarType, String> keys = {
    SnackbarType.success: 'success',
    SnackbarType.failed: 'failed',
    SnackbarType.info: 'info',
    SnackbarType.wait: 'wait',
  };

  static const Map<SnackbarType, String> values = {
    SnackbarType.success: 'Success',
    SnackbarType.failed: 'Failed',
    SnackbarType.info: 'Info',
    SnackbarType.wait: 'Wait',
  };

  String? get key => keys[this];
  String? get value => values[this];

  static SnackbarType? fromRaw(String raw) =>
      keys.entries.firstWhere((e) => e.value == raw).key;

  static SnackbarModel style(SnackbarType? snackbarType) {
    var snackbarModel;
    switch (snackbarType) {
      case SnackbarType.failed:
        snackbarModel = SnackbarModel.failed();
        break;
      case SnackbarType.info:
        snackbarModel = SnackbarModel.info();
        break;
      case SnackbarType.success:
        snackbarModel = SnackbarModel.success();
        break;
      case SnackbarType.wait:
        snackbarModel = SnackbarModel.wait();
        break;
      default:
        snackbarModel = SnackbarModel.wait();
    }

    return snackbarModel;
  }
}
