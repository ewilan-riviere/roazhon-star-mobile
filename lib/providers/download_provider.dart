import 'dart:io';

import 'package:roazhon_star/models/application/DownloadResponse.dart';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

class DownloadProvider {
  static var httpClient = HttpClient();
  Future<DownloadResponse> downloadFile({
    required String url,
    String? directory,
    String? filename,
  }) async {
    print('Start download...');
    var downloadResponse = DownloadResponse(success: false);
    try {
      var request = await httpClient.getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var headers = response.headers;
      filename ??= headers
          .value('content-disposition')
          .toString()
          .replaceAll('attachment; filename=', '');
      print(directory);
      directory ??= (await path_provider.getTemporaryDirectory()).path;
      print(directory);
      // var dir = await DownloadProvider.getExternalStorageDownloadDirectory();
      var file = File('$directory/$filename');
      await file.writeAsBytes(bytes);
      downloadResponse.success = true;
      downloadResponse.path = directory;
      downloadResponse.filename = filename;

      print('End of download.');
      // await OpenFile.open(file.path);

      return downloadResponse;
    } catch (e) {
      print('Error on download.');
      print(e.toString());
    }

    return downloadResponse;
  }
}
