# Roazhon Star

[![flutter](https://img.shields.io/static/v1?label=Flutter&message=v2.2&color=02569B&style=flat-square&logo=flutter&logoColor=ffffff)](https://flutter.dev)
[![dart](https://img.shields.io/static/v1?label=Dart&message=v2.13&color=0175C2&style=flat-square&logo=dart&logoColor=ffffff)](https://dart.dev)
[![dart](https://img.shields.io/static/v1?label=Dart&message=Null%20Safety&color=0175C2&style=flat-square&logo=dart&logoColor=ffffff)](https://dart.dev/null-safety/migration-guide)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

```bash
https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-topologie-lignes-tr/records?limit=100&pretty=false&timezone=UTC&sort=id

https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-topologie-parcours-td/records?refine=nomcourtligne:64&rows=10&pretty=false&timezone=UTC

https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-circulation-passages-tr/records?search=acigne&sort=depart&refine=nomcourtligne%3A64&refine=sens%3A1&rows=10&select=*&pretty=false&timezone=UTC

https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-topologie-parcours-td/records?refine=nomcourtligne:64&rows=10&pretty=false&timezone=UTC



https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-circulation-passages-tr/records?search=acigne&sort=depart&refine=nomcourtligne%3A64&refine=sens%3A1&rows=10&select=*&pretty=false&timezone=UTC


// list of bus lines
https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-topologie-lignes-td/records?limit=10
// detail bus line 240d5ed4d5bd32c4c21f960b3e4ff2c1bc3ea81c
https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-topologie-lignes-td/records/240d5ed4d5bd32c4c21f960b3e4ff2c1bc3ea81c

https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-circulation-passages-tr/records?refine=nomcourtligne:64
https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-circulation-passages-tr/records?refine=idligne:240d5ed4d5bd32c4c21f960b3e4ff2c1bc3ea81c


https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-lignes-pictogrammes-dm&q=&sort=idligne&facet=nomcourtligne&facet=date&facet=resolution
https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-lignes-pictogrammes-dm/records?q=&sort=idligne&facet=nomcourtligne&facet=date&facet=resolution&refine=nomcourtligne:64

https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-topologie-dessertes-td&q=&facet=libellecourtparcours&facet=nomcourtligne&facet=nomarret&facet=estmonteeautorisee&facet=estdescenteautorisee/f2ada94d490c5545062f36c8f18827063da8daab
https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-topologie-dessertes-td?q=&facet=libellecourtparcours&facet=nomcourtligne&facet=nomarret&facet=estmonteeautorisee&facet=estdescenteautorisee/f2ada94d490c5545062f36c8f18827063da8daab

https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-topologie-lignes-td/records?search=c1&rows=10&pretty=false&timezone=UTC

https://data.explore.star.fr/api/v2/catalog/datasets/tco-bus-topologie-parcours-td/records?refine=nomcourtligne:c1&rows=10&pretty=false&timezone=UTC
```
